var AWS = require('aws-sdk');
var fs = require('fs');

// AWS credentials (same as Proxy Services)
const AWS_ACCESS_KEY_ID = process.env.AWS_ACCESS_KEY_ID || 'AKIAIEPBWQWK4FNKZSRA';
const AWS_SECRET_ACCESS_KEY = process.env.AWS_SECRET_ACCESS_KEY || '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';
const AWS_DEFAULT_REGION = process.env.AWS_DEFAULT_REGION || 'eu-west-1';

const APP_ID = process.env.APP_ID || '97108C1C-C09C-4CED-97B2-47759820F60E'; // should be BETHEWONE
const TABLE = process.env.AWS_TABLE || 'Demo_Users'; // default : demo environment

const FORCE = process.env.FORCE || 0;
const MODE = process.env.MODE || 'count'; // default : no update (count is safe)
const TIME = 10; // milliseconds to wait before each update request (and avoid to reach the current DynamoDB limits)

var users_total = 0; // total of users
var users = []; // set of users to update

console.log('Updating AWS configuration...');
AWS.config.update({
    accessKeyId: AWS_ACCESS_KEY_ID,
    region: AWS_DEFAULT_REGION,
    secretAccessKey: AWS_SECRET_ACCESS_KEY
});
console.log('AWS configuration : UPDATED');

var dynamodb = new AWS.DynamoDB();
dynamodb.listTables({}, (err, data) => {
    if (err) {
        console.error(err);
    } else {
        console.log('💡','tables: ',data.TableNames);
    }
});

var docClient = new AWS.DynamoDB.DocumentClient();

if (MODE !== 'update') {
    console.log("### Counting the non updated entries ###");
} else {
    console.warn("### Updating the non updated entries ###");
}

console.log(`Scanning all Users in [${TABLE}] table`);
var params_scan = {
    TableName: TABLE
}
docClient.scan(params_scan, onScan);

function onScan(err, data) {
    if (err) {
        console.error(`Unable to scan the table [${TABLE}]. Error JSON:${JSON.stringify(err, null, 2)}`);
    } else {
        users_total += data.Items.length;

        data.Items.forEach(function (user) {
            if (!user.app_id || FORCE) {
                users.push({
                    phone: user.phone,
                    gamer_id: user.gamer_id // can be undefined if nothing is provided
                });
            }
        });
        console.log(`${data.Items.length} scanned users / total = ${users_total} / ${users.length} will be updated`);

        // continue to scan if there are more items
        // the response size is limited (1MB)
        if (typeof data.LastEvaluatedKey != 'undefined') {
            params_scan.ExclusiveStartKey = data.LastEvaluatedKey;
            docClient.scan(params_scan, onScan);
        }
        else { // last request, all items should have been received now
            if (users.length) {
                console.log(`${users.length} Users to UPDATE`);
                if (MODE === 'update') {
                    console.log('Updating the Users...');
                    for (var updates = 0; updates < users.length; updates++) {
                        var user_update = users[updates];
                        (function (u, i, max) {
                            new Promise((resolve, reject) => {
                                setTimeout(function () {
                                    update(u).then(
                                        (result) => resolve(result),
                                        (err) => reject(err) // stop the update
                                    );
                                }, TIME * i);
                            }).then(
                                (result) => console.log(`${i + 1}/${max + 1} - ${(i !== max) ? result : 'No more user to update 👍'}`),
                                (err) => { throw new Error(err); }
                            );
                        })(user_update, updates, users.length - 1);
                    }
                } else {
                    console.log('Saving the phone of the user to update in a local file \"users.json\"');
                    fs.writeFileSync('export/users.json', JSON.stringify(users, null, 2)); // just for information
                }
            } else { // nothing to update
                console.warn('No user to update');
            }
        }
    }
};

function update(user) {
    return new Promise(function (resolve, reject) {
        const params_update = {
            TableName: TABLE,
            Key: {
                "phone": user.phone
            },
            UpdateExpression: "set app_id = :app_id",
            ExpressionAttributeValues: {
                ":app_id": APP_ID
            },
            ReturnValues: "ALL_NEW"
        }
        docClient.update(params_update, function (err, data) {
            if (err) {
                console.error("Unable to update item. Error JSON:", JSON.stringify(err, null, 2));
                reject(Error(err));
            } else {
                create(data.Attributes).then(
                    (result) => resolve(result),
                    (err) => reject(err)
                );
            }
        });
    });

}

function create(user) {
    return new Promise(function (resolve, reject) {
        const params_create = {
            TableName: TABLE + "_new",
            Item: user
        }
        docClient.put(params_create, function (err, data) {
            if (err) {
                console.error("Unable to create item. Error JSON:", JSON.stringify(err, null, 2));
                reject(Error(err));
            } else {
                resolve(`LAST created : ${JSON.stringify(user)}`);
            }
        });
    });

}


