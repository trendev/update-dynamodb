# Add app_id field and update DynamoDB with BETHEWONE's app_id
> Author : julien.sie@di-mob-a.com

## Using on nodejs
### Count the entries to update
`AWS_TABLE=your_table_name node server.js`

### Update the entries
`AWS_TABLE=your_table_name MODE=update node server.js`

## Using with docker
### Build the image
`docker build -t dimobahub/upd8dynamodb .`
### Count the user to update in the Users table
`docker run -it --rm -v $PWD/export:/usr/src/app/export -e AWS_TABLE="Users" dimobahub/upd8dynamodb`
### Update the entries
`docker-compose up`
#### remove the stopped container and the fresh build image
`docker-compose down --rmi all`
